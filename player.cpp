#include "player.h"
#include <cstdio>
#include <time.h>

int nmoves_self = 0;
int nmoves_op = 0;
int alpha = -64 * 3;
int beta = 64 * 3;

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    // wanted to add an integer variable here so we don't have to keep 
    // commenting out our various methods, but when I did it failed.
    
    // Initializing board
    bd = new Board();
    sd = side;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */
     
     /*fprintf(stderr, "msleft: %d\n", msLeft);
     // Start time and end time
     time_t start;
     
     // Start timer
     time (&start);
     
     while (start < msLeft * 0.001 && msLeft != -1)
     {
         fprintf(stderr, "Testing!\n");
     }*/
     
     Side opposing = (sd == BLACK) ? WHITE : BLACK;
     int method = 3;
     
     // make sure time less than msLeft
     
     // First, process the opponent's move
     bd->doMove(opponentsMove, opposing);
     
     // Check if game is over
     if (bd->isDone())
     {
         return NULL;
     }
     
     std::vector<Move *> moves_self = possible_moves(bd, sd);
     std::vector<Move *> moves_op = possible_moves(bd, opposing);
     
     // Calculate number of possible moves from given configuration
     // Check if any possible moves left
     if (bd->hasMoves(sd))
     {
         nmoves_self = moves_self.size();
     }
     
     if (bd->hasMoves(opposing))
     {
         nmoves_op = moves_op.size();
     }
     
     // PART 1: BASIC ITERATION
     if (method == 1 && bd->hasMoves(sd))
     {
         // Calculate own move...at this stage, some arbitrary allowed move
         for (int i = 0; i < 8; i++)
         {
             for (int j = 0; j < 8; j++)
             {
                 Move move(i, j);
                 if (bd->checkMove(&move, sd))
                 {
                     //fprintf(stderr, "Found a move: (%d, %d)\n", i, j);
                     bd->doMove(&move, sd);
                     Move * fin_move = new Move(i, j);
                     return fin_move;
                 }
             }
        }
     }
     
     
     // PART 2: HEURISTIC FUNCTION
     int fin_score = -64 * 3;
     int temp_score = 0;
     //Move * fin_move = new Move(0, 0);
     Move * fin_move;
     //std::vector<Move> Player::possible_moves(Board * bd, Side side)
     
     if (method == 2 && bd->hasMoves(sd))
     {
         std::vector<Move*> poss_moves = possible_moves(bd, sd);
         for (unsigned int i = 0; i < poss_moves.size(); i++)
         {
             Board * copy_bd = bd->copy();
             copy_bd->doMove(poss_moves[i], sd);
             
             temp_score = score(copy_bd, sd, poss_moves[i]);
             
             if (temp_score > fin_score)
             {
                 fin_score = temp_score;
                 fin_move = poss_moves[i];
                 
             }
             delete copy_bd;
         }
         bd->doMove(fin_move, sd);
         return fin_move;
     }
     
         
     // PART3: Minimax
     // first make a list of all possible moves
     std::vector<Move *> moves;
     if (method == 3 && bd->hasMoves(sd))
     {
         moves = possible_moves(bd, sd);
         Move * fin_move = next_level(moves, bd, sd);
         bd->doMove(fin_move, sd);
         return fin_move;
     }
     
     // Minimize frontier discs (discs adjacent to empty squares)
     
     // now we have a list of the moves
     

    return NULL;
    
}
/*
Move *Player::minimax(Board *bd, Side side, int count)
{
	std::vector<Move *> moves = possible_moves(bd, side);
	Move * fin_move = next_level(moves, bd, side);
	bd->doMove(fin_move, side);
	return fin_move;
}
* */


Move *Player::next_level(std::vector<Move *> moves, Board *bd, Side side)
{
	// this function takes in the list of the vector and the current 
	// state of the board, and then returns the Move* that should be
	// played. 
	
	// make a vector of the minimum scores, and then find the index 
	// of the maximum, and return that Move *.
	
	std::vector<int> scores;
	std::vector<int> del;
	
	for(unsigned int i = 0; i < moves.size(); i++)
	{
		scores.push_back(min_score(moves[i], bd, side));	
	}
	
	// delete the ones  corresponding to NULL in moves
	for(int j = del.size()-1; j >= 0; j--)
	{
		moves.erase(moves.begin()+del[j]);
	}
	
	// now we have a vector that has corresponding minimum scores
	// need to find index of maximum score to return the move
	
	int max_index = 0;
	int max = scores[0];
	
	for(unsigned int j = 1; j < moves.size(); j++)
	{
		if(max < scores[j])
		{
			// update max
			max = scores[j];
			max_index = j;
		}
	}
	// now max_index is index of maximum min score
	Move *max_move = moves[max_index];
	Move * fin_move = new Move(max_move->x, max_move->y);
	
    // fprintf(stderr, "\nMove: (%d, %d) Score: %d\n", fin_move->x, fin_move->y, max);
    
	return fin_move;
}

int Player::min_score(Move *move, Board *bd, Side side)
{
	// makes a copy of the board, and simulates to get the lowest 
	// possible score, and then returns that score. 
	
	// loop through all moves, and save the minimum score, updating 
	// when there's another score that's smaller
	
	// each time you make a move on the board, make a copy first
	Board * bd_move = bd->copy();
	bd_move->doMove(move, side);
	
	// now see all possible moves
	// need to change sides
	Side opp = (side == BLACK) ? WHITE : BLACK;
	
	std::vector<Move *> moves;
	
	// loop through whole board to find all possible moves
	if(bd_move->hasMoves(opp))
	{
        moves = possible_moves(bd_move, opp);
	}
	else
	{
		return 0;
	}
	
	/*
	
	fprintf(stderr, "Move (%d, %d): ", move->x, move->y);
	for(unsigned int k = 0; k < moves.size(); k++)
	{
		fprintf(stderr, "(%d, %d) ", moves[k]->x, moves[k]->y);
	}
	fprintf(stderr, "\n");
	* 
	* */
	
	// loop through all of the moves, keeping min
	int min = model(moves[0], bd_move, opp); 
	int current;
	for(unsigned int i = 1; i < moves.size(); i++)
	{
		current = model(moves[i], bd_move, opp);
		
		// fprintf(stderr, "%d ", current);
		
		if(current < min)
		{
			min = current;
		}
	}
	
	// now min should reflect the minimum score
	
	bd_move->~Board();
	
	// fprintf(stderr, " Min Score: %d\n", min);
	
	return min;
}

int Player::model(Move *move, Board *bd_move, Side opp)
{
	// calculates the score for the side after making this move
	// creates a copy of the board
	Board * bd_move2 = bd_move->copy();
	
	// now that we've made the copy, let's make the move
	bd_move2->doMove(move, opp);
	
	// now calculate the score of side that isn't opp
	// switch sides
	Side side = (opp == BLACK) ? WHITE : BLACK;
	
	int score = bd_move2->count(side) - bd_move2->count(opp);
	
	bd_move2->~Board();
	return score;
}

std::vector<Move*> Player::possible_moves(Board * bd, Side side) 
{
    std::vector<Move *> moves; 
	for (int i = 0; i < 8; i++) 
	{ 
		for (int j = 0; j < 8; j++) 
		{ 
			Move *move = new Move(i, j); 
			if (bd->checkMove(move, side)) 
			{ 
				moves.push_back(move); 
			} 
		} 
	} 
	
	
	return moves; 
}

int Player::score(Board * bd, Side side, Move * move)
{
    int score = 0;
    Side opposing = (sd == BLACK) ? WHITE : BLACK;
    
    score = bd->count(side) - bd->count(opposing);
             
    if ( (move->getX() == 0 && move->getY() == 0) 
    || (move->getX() == 0 && move->getY() == 7) 
    || (move->getX() == 7 && move->getY() == 0) 
    || (move->getX() == 7 && move->getY() == 7))
    {
        score *= 3;
    }
             
    if ( (move->getX() == 0 && move->getY() == 1) 
    || (move->getX() == 1 && move->getY() == 0) 
    || (move->getX() == 0 && move->getY() == 6) 
    || (move->getX() == 6 && move->getY() == 0) 
    || (move->getX() == 1 && move->getY() == 7) 
    || (move->getX() == 7 && move->getY() == 1) 
    || (move->getX() == 6 && move->getY() == 7) 
    || (move->getX() == 7 && move->getY() == 6))
    {
        score *= -3;
    }
    
    return score;
}
