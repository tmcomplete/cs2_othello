at the beginning of the Player::doMove method, there is the variable
int method = 3; 
I set 3 as the default to use the minimax method. 
If you would like to try the most basic method, set it to 1. 
If you would like to try the simple heuristic, set it to 2.
For minimax, obviously set it to 3. 
If you set it to anything else, it won't do anything.

-----------------------------------------------------------------------

Both group members contributed equally. Both were very good with 
communicating about updates to code, when they were pushing, what they 
were changing, any problems they had, etc. For the first week, Betsy 
worked on getting the AI to play any valid move without error and the 
heuristic algorithm. Aileen worked on implementing the minimax algorithm.
Both group members provided feedback on how to improve code. Aileen 
suggested that helper functions be used and both members helped write 
them to clean up the code. For the second week, both members discussed 
possible ways to improve the AI based on the information given in the 
assignment and via the internet. Both members checked if the AI could 
beat ConstantTimePlayer on their computers and concluded that the 
minimax algorithm could sometimes beat ConstantTimePlayer, but not 
consistently enough.

Our group attempted to implement alpha-beta pruning to make our AI 
tournament-worthy. This reduces the run time of the AI so that it is 
more efficient and thus can do more in the given time. In addition, 
attempts were made to keep track of the number of possible moves for 
both sides for a given configuration of the board. We would have liked 
to also keep track of the number of pioneer discs (discs adjacent to 
empty spaces), as this would be important to determining whether some 
move is beneficial for the player or not. However, we did not have 
enough time to fully implement this.Attempts were also made to make 
the AI return NULL if the time limit had already elapsed, but this was 
also not implemented fully before the assignment was due. Its code in 
doMove is thus commented out so that it does not interfere with the
rest of the code.

We also tried to have 4 layers of minimax instead of 2, but we didn't 
have time to do this wihtout fixing errors. Therefore, we just 
commented it out. 
