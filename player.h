#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <vector>
#include "common.h"
#include "board.h"
using namespace std;

class Player {
    
    Side sd;

public:
	Board * bd;
    Player(Side side);
    ~Player();
    
    
    Move *doMove(Move *opponentsMove, int msLeft);
    Move *next_level(std::vector<Move *> moves, Board *bd, Side side);
    int min_score(Move *move, Board *bd, Side side);
    int model(Move *move, Board *bd_move, Side opp);
    std::vector<Move*> possible_moves(Board * bd, Side side);
    int score(Board * bd, Side side, Move * move);
    //Move *minimax(Board *bd, Side side, int count);
    

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
